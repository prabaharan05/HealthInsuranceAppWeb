package com.insurance.service;

import java.math.BigDecimal;

import com.insurance.model.InsuranceApplication;

public interface InsuranceService {
	
	BigDecimal doApplicationProcess(InsuranceApplication insuranceApplication);

}
