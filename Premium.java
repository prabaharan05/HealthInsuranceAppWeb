package com.insurance.model;

import java.math.BigDecimal;

public class Premium {
	
	private BigDecimal baseAmt=new BigDecimal(5000);

	public BigDecimal getBaseAmt() {
		return baseAmt;
	}

	public void setBaseAmt(BigDecimal baseAmt) {
		this.baseAmt = baseAmt;
	}
	

}
