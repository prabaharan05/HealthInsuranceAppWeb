package com.insurance.controller;

import java.math.BigDecimal;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.insurance.model.InsuranceApplication;
import com.insurance.service.InsuranceService;

@RestController
@RequestMapping("/insurance")
public class InsuranceController {

	private InsuranceService insuranceService;
	
	@RequestMapping(value="/generateqoute",method=RequestMethod.POST)
	public BigDecimal generateQuoute(@RequestBody InsuranceApplication insuranceApplication) {
		return insuranceService.doApplicationProcess(insuranceApplication);

	}
	


	public InsuranceService getInsuranceService() {
		return insuranceService;
	}

	public void setInsuranceService(InsuranceService insuranceService) {
		this.insuranceService = insuranceService;
	}

}
