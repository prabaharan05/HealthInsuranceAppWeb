/**
 * 
 */
InsureApp.controller('InsuranceController', [ 'InsuranceService','$log',
		function(InsuranceService,$log) {

			self = this;
			self.insuranceApplication = {
				applicantId : 0,
				applicantName : '',
				age : null,
				gender : '',
				currentHealth : null,
				habits : null,
				applicantPremium:null
			};
			self.result=null;
			self.currentHealth={hyperTension:false,bloodPressure:false,bloodSugar:false,overweight:false};
			self.habits={smoking:false,alcohol:false,dailyExercise:false,drugs:false};
			
			self.generateQoute=function(){
			
				self.insuranceApplication.currentHealth=self.currentHealth;
				self.insuranceApplication.habits=self.habits;
				InsuranceService.generateQoute(self.insuranceApplication).then(function(data){
					self.result=data;
					
				},function(err){
					console.error('Insurance Quoate is not generated!');
				});
			}

					} ]);