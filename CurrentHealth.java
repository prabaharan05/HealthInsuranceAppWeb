package com.insurance.model;

public class CurrentHealth {

	private boolean hyperTension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overweight;
	public boolean isHyperTension() {
		return hyperTension;
	}
	public void setHyperTension(boolean hyperTension) {
		this.hyperTension = hyperTension;
	}
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public boolean isOverweight() {
		return overweight;
	}
	public void setOverweight(boolean overweight) {
		this.overweight = overweight;
	}
	
	
	
}
