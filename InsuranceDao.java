package com.insurance.dao;

import com.insurance.model.InsuranceApplication;

public interface InsuranceDao {
	
	void saveInsuranceApplicant(InsuranceApplication insuranceApplication);
}
