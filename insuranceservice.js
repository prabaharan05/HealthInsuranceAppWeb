/**
 * 
 */
var url='http://localhost:8080/HealthInsuranceAppWeb/insurance/';

InsureApp.factory('InsuranceService',['$q','$http',function($q,$http){
	return {
		
		generateQoute:function(insureApplication){
			var deffered=$q.defer();
			return $http.post(url+'generateqoute',insureApplication).then(function(resp){
				return resp.data;
			},function(errResp){
				return $q.reject(errResp);
			})
		}
		
	}
}]);