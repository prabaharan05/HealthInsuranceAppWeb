package com.insurance.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.insurance.dao.InsuranceDao;
import com.insurance.model.InsuranceApplication;
import com.insurance.model.Premium;

@Service
public class InsuranceServiceImpl implements InsuranceService {

	private Premium premium;
	private InsuranceDao insuranceDao;

	@Override
	public BigDecimal doApplicationProcess(InsuranceApplication insuranceApplication) {
		insuranceApplication.setApplicantPremium(doPremiumCalculation(insuranceApplication));
		//insuranceDao.saveInsuranceApplicant(insuranceApplication);
		
		return insuranceApplication.getApplicantPremium();
	}

	private BigDecimal doPremiumCalculation(InsuranceApplication insuranceApplication) {
		BigDecimal applicationAmt = new BigDecimal(0);
		BigDecimal percentage = getPercentage(insuranceApplication);
		premium=new Premium();
		System.out.println(premium.getBaseAmt()+"=="+percentage);
				
		if(percentage!=null&&premium.getBaseAmt()!=null) {
		applicationAmt = premium.getBaseAmt()
				.add((premium.getBaseAmt().multiply(percentage)).divide(new BigDecimal(100)));
		}
		return applicationAmt;
	}

	private BigDecimal getPercentage(InsuranceApplication insuranceApplication) {
		BigDecimal percentage = new BigDecimal(0);
		// based on the age percentage calucation
		 if (insuranceApplication.getAge() >= 18 && insuranceApplication.getAge() < 40) {

			percentage.add(new BigDecimal(10));

		} else if (insuranceApplication.getAge() >= 40) {
			percentage.add(new BigDecimal(20));

		}
		 //Based on gender
		 if(insuranceApplication.getGender().equalsIgnoreCase("male"))
			 percentage.add(new BigDecimal(2));
		 
		// Current Health percentage calucation
		if (insuranceApplication.getCurrentHealth().isBloodPressure())
			percentage.add(new BigDecimal(1));
		if (insuranceApplication.getCurrentHealth().isBloodSugar())
			percentage.add(new BigDecimal(1));
		if (insuranceApplication.getCurrentHealth().isBloodPressure())
			percentage.add(new BigDecimal(1));
		if (insuranceApplication.getCurrentHealth().isBloodSugar())
			percentage.add(new BigDecimal(1));
		
		// Habits percentage calucation
		if (insuranceApplication.getHabits().isAlcohol())
			percentage.add(new BigDecimal(3));
		if (insuranceApplication.getHabits().isDrugs())
			percentage.add(new BigDecimal(3));
		if (insuranceApplication.getHabits().isSmoking())
			percentage.add(new BigDecimal(3));
		if (insuranceApplication.getHabits().isDailyExercise())
			percentage.subtract(new BigDecimal(3));

		return percentage;

	}

	public Premium getPremium() {
		return premium;
	}

	public void setPremium(Premium premium) {
		this.premium = premium;
	}

	public InsuranceDao getInsuranceDao() {
		return insuranceDao;
	}

	public void setInsuranceDao(InsuranceDao insuranceDao) {
		this.insuranceDao = insuranceDao;
	}

}
