
import com.insurance.model.CurrentHealth;
import com.insurance.model.Habits;
import com.insurance.model.InsuranceApplication;
import com.insurance.service.InsuranceService;
import com.mkyong.examples.spring.AppConfig;
import com.mkyong.examples.spring.DataModelService;
import com.mkyong.examples.spring.MachineLearningService;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class InsuranceTest {

	//DI
    @Autowired
    InsuranceService insuranceService;

    @Test
    public void quoateGenetratedTest() {

    	InsuranceApplication application=new InsuranceApplication();
    	application.setAge(34);
    	application.setApplicantName("James");
    	application.setGender("Male");
    	CurrentHealth health=new CurrentHealth();
    	health.setBloodPressure(false);
    	health.setBloodSugar(false);
    	health.setHyperTension(false);
    	health.setOverweight(true);
    	application.setCurrentHealth(health);
    	
    	Habits habits=new Habits();
    	habits.setAlcohol(true);
    	habits.setDailyExercise(true);
    	habits.setDrugs(false);
    	habits.setSmoking(false);
    	assertNotNull(application.getAge());	
    	assertNotNull(application.getGender());	
        assertEquals(6859, insuranceService.doApplicationProcess(application));

    }
}
