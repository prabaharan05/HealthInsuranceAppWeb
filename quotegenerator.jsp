<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="insureApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insurance Application</title>
</head>
<body ng-controller="InsuranceController as ctrl">
<h2>Insurance Quotation</h2>
<div  >
  <form  >
    Name: <input type="text" ng-model="ctrl.insuranceApplication.applicantName" ><br>
    Gender: <input type="text" ng-model="ctrl.insuranceApplication.gender" ><br>
    Age:<input type="number" ng-model="ctrl.insuranceApplication.age" >
   <h3>Current Health Status</h3>
  <label>Hypertension:
    <input type="checkbox" ng-model="ctrl.currentHealth.hyperTension">
  </label><br/>
  <label>Blood pressure:
    <input type="checkbox" ng-model="ctrl.currentHealth.bloodPressure"
           >
   </label><br/>
   <label>Blood sugar:
    <input type="checkbox" ng-model="ctrl.currentHealth.bloodSugar">
  </label><br/>
  <label>Overweight:
    <input type="checkbox" ng-model="ctrl.currentHealth.overweight"
           >
   </label><br/>
   <h3>Habits</h3>
  <label>Smoking:
    <input type="checkbox" ng-model="ctrl.habits.smoking">
  </label><br/>
  <label>Alcohol:
    <input type="checkbox" ng-model="ctrl.habits.alcohol"
           >
   </label><br/>
   <label>Daily exercise:
    <input type="checkbox" ng-model="ctrl.habits.dailyExercise">
  </label><br/>
  <label>Drugs:
    <input type="checkbox" ng-model="ctrl.habits.drugs"
           >
   </label><br/>
   <input type="button"  ng-click="ctrl.generateQoute()" value="Submit">
  </form>
 
</div>
<h2>Output</h2>
<h3>{{}}</h3>
</body>
<script src="${pageContext.request.contextPath}/angularjs/angular.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap.min.js"></script> -->
<script src="${pageContext.request.contextPath}/angularjs/insuranceapp.js"></script>
<script src="${pageContext.request.contextPath}/angularjs/insurancecontroller.js"></script>
<script src="${pageContext.request.contextPath}/angularjs/insuranceservice.js"></script>

</html>