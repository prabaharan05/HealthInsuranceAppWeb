package com.insurance.model;

import java.math.BigDecimal;

public class InsuranceApplication {
	
	private int applicantId;
	private String applicantName;
	private int age;
	private String gender;
	private CurrentHealth currentHealth;
	private Habits habits;
	private BigDecimal applicantPremium=new BigDecimal(0);

	
	public int getApplicantId() {
		return applicantId;
	}
	public void setApplicantId(int applicantId) {
		this.applicantId = applicantId;
	}
	public String getApplicantName() {
		return applicantName;
	}
	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	public BigDecimal getApplicantPremium() {
		return applicantPremium;
	}
	public void setApplicantPremium(BigDecimal applicantPremium) {
		this.applicantPremium = applicantPremium;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	

}
